# Systemd unit for automounting usb devices over usbip

This is shamelessly ripped from [larsks sysetemd-usb-gadget][https://github.com/larsks/systemd-usb-gadget]

## Installation

To install the systemd unit, run `make install` in the source
directory, which will place the support scripts into `/sbin` and the
systemd unit into `/etc/systemd/system`.

## Configuration

Create one or files in `/etc/usbip-device` named `<device_name>.conf`.
These files **must** contain the following configuration keys:


- `BUS_ID` -- usbip bus id (use usbip list -l). 

For example, to show usb devices:
# usbip list -l
 - busid 1-1 (0a89:000c)
   Aktiv : Guardant Stealth 3 Sign/Time (0a89:000c)

 - busid 1-2 (0a89:000c)
   Aktiv : Guardant Stealth 3 Sign/Time (0a89:000c)

To share a device with busid 1-1 create a file
called /etc/usbip-devices/guardant.conf

    BUS_ID=1-1

To enable the device at boot, run:

    systemctl enable usbip-device@mouse

To share the device immediately:

    systemctl start usbip-device@mouse

To stop sharing it:

    systemctl stop usbip-device@mouse
